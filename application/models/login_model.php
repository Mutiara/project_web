<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function getDataUser()
	{
		return $this->db
					->where('nama_kasir',$this->input->post('nama_kasir'))
					->where('password',$this->input->post('password'))
					->get('kasir');
	}

}

/* End of file login_model.php */
/* Location: ./application/models/login_model.php */