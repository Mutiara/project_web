
<div class="panel">
	<div class="panel-heading">
		<h3 class="panel-title">Data buku</h3>
	</div>
	<div class="panel-body">
		<?php if ($this->session->flashdata('pesan')!=null): ?>
			<div class="alert alert-info alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<i class="fa fa-times-circle"></i>
				<?=$this->session->flashdata('pesan')?>
			</div>
		<?php endif ?>
		<center><a href="#modalTambah" data-toggle="modal" class="btn btn-success">Tambah</a></center>
		<br>
		<table class="table table-bordered">
			<thead>
			
				<th><center>Id buku</center></th>
				<th><center>Judul buku</center></th>				
				<th><center>Tahun</center></th>
				<th><center>Harga</center></th>
				<th><center>Cover Foto</center></th>
				<th><center>Penerbit</center></th>
				<th><center>Penulis</center></th>
				<th><center>Stok</center></th>
				<th><center>Nama Kategori</center></th>
				<th><center>Aksi</center></th>

			</thead>
			<tbody>
				<?php foreach ($tampil_buku as $buku): ?>
					<tr>
						<td><center><?=$buku->id_buku?></center> </td>
						<td><center><?=$buku->judul_buku?></center> </td>	
						<td><center><?=$buku->tahun?></center> </td>	
						<td><center><?=$buku->harga?></center> </td>	
						<td><center><img src="<?=base_url()?>assets/img/<?=$buku->foto_cover?>" style="width: 100px"></center></td>
						<td><center><?=$buku->penerbit?></center> </td>
						<td><center><?=$buku->penulis?></center> </td>	
						<td><center><?=$buku->stok?></center> </td>	
						<td><center><?=$buku->nama_kategori?></center> </td>	
						<td><center>
							<a href="#edit" onclick="edit(<?=$buku->id_buku?>)" class="btn btn-primary" data-toggle="modal">Ubah</a>
							<a href="<?=base_url('index.php/buku/hapus/'.$buku->id_buku)?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin?')">Hapus</center></a>
						</td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		<div class="modal fade" id="modalTambah">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span><h4 class="modal-title">Tambah Barang</h4>
						</button>
					</div>
					<div class="modal-body">
						<form action="<?=base_url('index.php/buku/tambah_buku')?>" method="POST" enctype="multipart/form-data">
							<table class="table">
								<tr>
									<td>Judul Buku</td>
									<td><input type="text" name="judul_buku" class="form-control"><br>
									</tr>
									<tr>
										<td>Tahun</td>
										<td><input type="text" name="tahun" class="form-control"><br>
										</tr>
										<tr>
											<td>Harga</td>
											<td><input type="text" name="harga" class="form-control"><br>
											</tr>
											<tr>
												<td>Foto</td>
												<td><input type="file" name="foto_cover" class="form-control"><br>
												</tr>
												<tr>
													<td>Penerbit</td>
													<td><input type="text" name="penerbit" class="form-control"><br>
													</tr>
													<tr>
														<td>Stok</td>
														<td><input type="text" name="stok" class="form-control"><br>
														</tr>
														<tr>
															<td>Penulis</td>
															<td><input type="text" name="penulis" class="form-control"><br>
															</tr>
															<tr>
																<td>Kategori</td>
																<td>
																	<select name="kategori" class="form-control">
																		<?php foreach ($tampil_kategori as $kat): ?>
																			<option value="<?=$kat->id_kategori?>">
																				<?=$kat->nama_kategori?>
																			</option>
																		<?php endforeach ?>
																	</select>
																</td>
															</tr>
															<tr>
																<td><input type="submit" name="simpan" value="simpan" class="btn btn-success"></td>
															</tr>
														</table>
													</form>
												</div>
											</div>	
										</div>	
									</div>
									<div class="modal fade" id="edit">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span><h4 class="modal-title">Edit Barang</h4>
													</button>
												</div>
												<div class="modal-body">
													<form action="<?=base_url('index.php/buku/ubah')?>" method="POST" enctype="multipart/form-data">
														<table class="table">
															<tr>
																<td>Judul Buku</td>
																<td><input id="judul_buku" type="text" name="judul_buku" class="form-control"><br>
																</tr>
																<tr>
																	<td>Tahun</td>
																	<td><input id="tahun" type="text" name="tahun" class="form-control"><br>
																	</tr>
																	<tr>
																		<td>Harga</td>
																		<td><input id="harga" type="text" name="harga" class="form-control"><br>
																		</tr>
																		<tr>
																			<td>Foto</td>
																			<td><input type="file" name="foto_cover" class="form-control"><br>
																			</tr>
																			<tr>
																				<td>Penerbit</td>
																				<td><input id="penerbit" type="text" name="penerbit" class="form-control"><br>
																				</tr>
																				<tr>
																					<td>Stok</td>
																					<td><input id="stok" type="text" name="stok" class="form-control"><br>
																					</tr>
																					<tr>
																						<td>Penulis</td>
																						<td><input id="penulis" type="text" name="penulis" class="form-control"><br>
																						</tr>
																						<tr>
																							<td>Kategori</td>
																							<td>
																								<select name="kategori" class="form-control" id="id_kategori">
																									<?php foreach ($tampil_kategori as $kat): ?>
																										<option value="<?=$kat->id_kategori?>">
																											<?=$kat->nama_kategori?>
																										</option>
																									<?php endforeach ?>
																								</select>
																							</td>
																						</tr>		
																						<input type="hidden" name="id_buku" id="id_buku">
																						<td><input type="submit" name="simpan" value="simpan" class="btn btn-success"></td>
																					</table>
																				</form>
																			</div>
																		</div>	
																	</div>	
																</div>
															</div>
														</div>
<script type="text/javascript">
function edit(a) {
	$.ajax({
	type:"post",
	url:"<?=base_url()?>index.php/buku/edit_buku/"+a,
	dataType:"json",
	success:function(data){
	$("#id_buku").val(data.id_buku);
	$("#judul_buku").val(data.judul_buku);
	$("#tahun").val(data.tahun);
	$("#harga").val(data.harga);
	$("#penerbit").val(data.penerbit);
	$("#penulis").val(data.penulis);
	$("#stok").val(data.stok);
	$("#id_kategori").val(data.id_kategori);

		}
});
}
</script>